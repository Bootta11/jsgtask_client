class GlobalFiltersPlugin{
    install(Vue){
        Vue.filter('currency', function(value){
            try{
                return '$' + value.toFixed(2);
            }catch (e) {
                return value;
            }
        })
    }
}

export default new GlobalFiltersPlugin();
