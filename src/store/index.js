import Vue from 'vue'
import Vuex from 'vuex'

import { auth } from './auth.module';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    is_search_enabled: false,
    is_login_modal_open: false,
    is_modal_open: {},
    modal_change: false,
    colors: ['lightgrey', 'lightblue', 'aquamarine', 'lightcoral', 'wheat'],
    active_color: 'lightgrey',
    cart: [],
    modal_image: undefined
    // state_change: false
  },
  mutations: {
    toggleSearch(state){
      state.is_search_enabled = !state.is_search_enabled;
    },
    openModal(state, modal_id){
      state.modal_change = true;
      state.is_modal_open[modal_id] = true;
      state.modal_change = false;
    },
    closeModal(state, modal_id){
      state.modal_change = true;
      state.is_modal_open[modal_id] = false;
      state.modal_change = false;
    },
    changeActiveColor(state, color){
      if(state.colors.indexOf(color) >=0 ) return state.active_color = color;
      console.warn('Unknown color');
    },
    addToCart(state, item){
      state.cart.push(item);
    },
    clearCart(state){
      state.cart = [];
    },
    setModalImage(state,image_url){
      state.modal_image = image_url;
    }
  },
  actions: {
  },
  modules: {
    auth
  }
})
