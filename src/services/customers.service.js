import axios from "axios";
import authHeader from './auth-header.js';
import common from './common.js'

const API_URL = common.server_address + '/customers/';

class CustomersService {
    get(id) {
        return axios.get(API_URL + id, { headers: authHeader() });
    }

    getAll(params) {
        return axios.get(API_URL, { headers: authHeader(), params});
    }

    checkout(data){
        return axios.post(API_URL + 'checkout', data, { headers: authHeader() })
    }
}

export default new CustomersService();
