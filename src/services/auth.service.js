import axios from "axios";
import common from "./common.js";

const API_URL = common.server_address;

class AuthService {
    login(user){
        return axios.post(API_URL + '/login', {
            email: user.email,
            password: user.password
        }).then( response => {
            if(response && response.status === 200 && response.data.token){
                localStorage.setItem('user', JSON.stringify(response.data))
            }

            return response;
        })
    }

    logout() {
        localStorage.removeItem('user');
    }

    register(user) {
        return axios.post(API_URL + 'register', {
            email: user.email,
            password: user.password
        });
    }
}

export default new AuthService();
