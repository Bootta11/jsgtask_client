import axios from "axios";
import authHeader from './auth-header';
import {server_adress} from './common'

const API_URL = server_adress + '/users/';

class UserService {
    get(id) {
        return axios.get(API_URL + id, { headers: authHeader() });
    }

    getAll() {
        return axios.get(API_URL, { headers: authHeader() });
    }
}

export default new UserService();
