export default {
    server_address: (process.env.VUE_APP_SERVER_ADDRESS !== undefined ? process.env.VUE_APP_SERVER_ADDRESS : 'http://localhost:8000' )
}
