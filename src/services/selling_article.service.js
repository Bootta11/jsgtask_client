import axios from "axios";
import authHeader from './auth-header.js';
import common from './common.js'

const API_URL = common.server_address + '/selling_articles/';

class SellingArticleService {
    get(id) {
        return axios.get(API_URL + id, { headers: authHeader() });
    }

    getAll(params) {
        return axios.get(API_URL, { headers: authHeader(), params: params });
    }

    getPhoto(id){
        return axios.get(API_URL + id + '/photo', { headers: authHeader() })
    }

    getPhotoUrl(id){
        return API_URL + id + '/photo';
    }
}

export default new SellingArticleService();
