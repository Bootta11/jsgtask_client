import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faSpinner, faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import vClickOutside from 'v-click-outside'
import VueMeta from 'vue-meta';
import GlobalFiltersPlugin from './filters/GlobalFiltersPlugin'
import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload)

library.add(faSearch, faSpinner, faShoppingCart);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(vClickOutside);
Vue.use(GlobalFiltersPlugin);
Vue.use(VueMeta);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
